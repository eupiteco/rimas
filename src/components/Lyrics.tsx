import React from 'react'
import {RhymesProvider} from '../contexts/RhymesContext'
import {LineType, LyricType} from '../models/lyricModel'
import Line from './Line'
import munRa from '../assets/mun-ra.json'

const Lyric: React.FC = () => {
  const lyricData: LyricType = munRa

  return (
    <RhymesProvider rhymeList={lyricData.rhymes as string[]}>
      {lyricData?.lyric.map((line: LineType, index:number) => {
        return (
          <Line line={line} key={index} />
        )
      })}
    </RhymesProvider>
  )
}

export default Lyric
