import React, {useEffect} from 'react'
import {debounce} from '../utils'
import {FragmentType, LineType} from '../models/lyricModel'
import {useRhymes} from '../contexts/RhymesContext'

const Line: React.FC<{line:LineType}> = ({line}) => {
  const {rhymes} = useRhymes();

  useEffect(() => {
    const handleSelection = debounce(() => {
      console.log(document.getSelection())
    }, 500)

    document.addEventListener('selectionchange', handleSelection)

    return () => {
      document.removeEventListener('selectionchange', handleSelection)
    }
  },[])

  return(
    <p>
      {line.map((fragment: FragmentType, index:number) => {
        return fragment.rhyme ? <span key={index} style={{backgroundColor: rhymes[fragment.rhyme]}}>{fragment.text}</span> : fragment.text 
      })}
    </p>
  )
}
export default Line
