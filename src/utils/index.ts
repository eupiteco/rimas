export function debounce(
  func: (args?: any) => void,
  duration: number
) {
  let timeout: number | ReturnType<typeof setTimeout>
  
  return function (args: any) {
    const effect = () => {
      return func(args)
    }
    clearTimeout(timeout)
    timeout = setTimeout(effect, duration)
  }
}
