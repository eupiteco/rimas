export type FragmentType = {
  text: string;
  rhyme?: string;
}

export type LineType = FragmentType[]

export type LyricType = {
  lyric: LineType[]
  rhymes?: string[]
}
