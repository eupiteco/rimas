export type RhymesType = {
  [key: string]: string;
}
