import './App.css';
import Lyric from './components/Lyrics';

function App() {
  return (
    <div className="App">
      <Lyric />
    </div>
  );
}

export default App;
