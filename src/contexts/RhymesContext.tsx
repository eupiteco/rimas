import React, {createContext, ReactNode, useContext, useEffect, useState} from "react";

const RhymesContext = createContext({})

export const RhymesProvider: React.FC<{rhymeList: string[], children: ReactNode}> = ({rhymeList, children}) => {
  const [ rhymes, setRhymes ] = useState<any>({}) //TODO better type this, something like a dictionary with vriable size (rhymesModel.tsx)

  useEffect(() => {
    const defaultSaturation = "50%"
    const defaultLight = "80%"
    const goldenRatio = 1.618033988749

    const rhymesColorscheme = rhymeList.reduce<any>((acc, cur, i): any => {
      const angle = 60 * i * goldenRatio % 360
      acc[cur] = `hsl(${angle} ${defaultSaturation} ${defaultLight}`
      return acc
    }, {}) 

    setRhymes(rhymesColorscheme)
  }, [rhymeList])

  return (
    <RhymesContext.Provider value={{rhymes}}>
      {children}
    </RhymesContext.Provider>
  )
}

export function useRhymes(): any {
  const context = useContext(RhymesContext)
  if (!context) throw new Error("useRhymes must be used within a RhymesProvider");
  return context
}
